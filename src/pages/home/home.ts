import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { AES256 } from '@ionic-native/aes-256';
import { File } from '@ionic-native/file';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';

declare var cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  str: string;
  lon: number;
  lat: number;
  secureKey: string = '12345678910123456789012345678901'; // Any string, the length should be 32
  secureIV: string = '1234567891123456'; // Any string, the length should be 16

  en: string;
  dec: string;

  options: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  constructor(
    private file: File,
    private aes256: AES256,
    private platform: Platform,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public navCtrl: NavController) {

    console.log('platform.ready start');
    // this.generateSecureKeyAndIV();
    // let data = "test";
    // this.encryptData();
    // let encryptedData = "2Q6GMYmFdBcdKbYNfiXNUQ==";
    console.log('BASBUG dir: ' + this.file.dataDirectory);

    console.log('get coordinates.');
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.lon = resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });

    console.log('get reverse location.');
    this.nativeGeocoder.reverseGeocode(52.5072095, 13.1452818, this.options)
      .then((result: NativeGeocoderReverseResult[]) => {
        console.log(JSON.stringify(result[0]));
        console.log('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*')
        console.log(JSON.stringify(result));
      })
      .catch((error: any) => console.log('reverse error: ' + error));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.str = 'test test test test';
  }

  setMessage() {
    console.log('set message method started.');
    this.nativeGeocoder.reverseGeocode(this.lat, this.lon, this.options)
      .then((result: NativeGeocoderReverseResult[]) => {
        console.log(JSON.stringify(result[0]));
        if (result[0].countryCode === 'TR' && result[0].administrativeArea === 'İstanbul') {
          this.str = 'Longitude: ' + this.lon + ' Latitude: ' + this.lat + ' is in İstanbul.';
        } else {
          this.str = 'Longitude: ' + this.lon + ' Latitude: ' + this.lat + ' is NOT in İstanbul.';
        }
      })
      .catch((error: any) => console.log('reverse error: ' + error));
  }

  async generateSecureKeyAndIV() {
    this.secureKey = await this.aes256.generateSecureKey('12345'); // Returns a 32 bytes string
    this.secureIV = await this.aes256.generateSecureIV('12345'); // Returns a 16 bytes string
  }

  encryptData() {
    this.aes256.encrypt('123456789101234567890123456789011', '1234567891123456', 'data')
      .then(res => {
        console.log('Encrypted Data : ' + JSON.stringify(res));
        this.en = res;
        this.decryptData();
      })
      .catch((error: any) => console.error(error));
    // this.en = await this.aes256.encrypt(this.secureKey, this.secureIV, 'testdata');
  }

  decryptData() {
    //this.aes256.decrypt('123456789101234567890123456789011', '1234567891123456', '8IHyWNKtVc+rnfwwQR8caQ==')
    this.aes256.decrypt('123456789101234567890123456789011', '1234567891123456', this.en)
      .then(res => {
        console.log('Encrypted Data : ' + JSON.stringify(this.en));
        console.log('Decrypted Data : ' + JSON.stringify(res));
        this.dec = res;
      })
      .catch((error: any) => console.error(error));
    // this.dec = await this.aes256.decrypt(this.secureKey, this.secureIV, this.en);
  }

  encrypt(secureKey, secureIV, data) {
    this.platform.ready().then(() => {
      cordova.plugins.AES256.encrypt(secureKey, secureIV, data,
        (encrypedData) => {
          console.log('Encrypted Data----' + JSON.stringify(encrypedData));
          this.en = encrypedData;
        }, (error) => {
          console.log('Error----' + JSON.stringify(error));
        });
    });
  }

  decrypt(secureKey, secureIV, encryptedData) {
    this.platform.ready().then(() => {
      cordova.plugins.AES256.encrypt(secureKey, secureIV, encryptedData,
        (decryptedData) => {
          console.log('Decrypted Data----' + JSON.stringify(this.decryptData));
          this.dec = decryptedData;
        }, (error) => {
          console.log('Error----' + JSON.stringify(error));
        });
    });
  }

  // readPage() {
  //   this.http.request('../assets/chapters/1/page_1.txt')
  //     .map(res => res.text())
  //     .subscribe(text => {
  //       this.str = text;
  //     })
  //     .catch(error => {
  //       console.log("Path not found with error:", error);
  //     });
  // }

}
